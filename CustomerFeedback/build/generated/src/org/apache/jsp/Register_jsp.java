package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Register_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Registration</title>\n");
      out.write("       <style type=\"text/css\">\n");
      out.write("            body{\n");
      out.write("                background: #54943E ; \n");
      out.write("            }\n");
      out.write("            #header{\n");
      out.write("                width: 80%;\n");
      out.write("                height: 100%;\n");
      out.write("                background:#16A085;\n");
      out.write("            }\n");
      out.write("            #container{\n");
      out.write("                width: 80px; padding:20px;\n");
      out.write("                height: 150px;\n");
      out.write("            }\n");
      out.write("            ul {list-style: none;padding: 0px;margin: 0px;}\n");
      out.write("            ul li {display: block;position: relative;float: left;border:1px solid #000}\n");
      out.write("            li ul {display: none;}\n");
      out.write("            ul li a {display: block;background: #000;padding: 16px 10px 3px 271px;text-decoration: none;\n");
      out.write("                     white-space: nowrap;color: #fff;}\n");
      out.write("            ul li a:hover {background: #1F531A   ;}\n");
      out.write("            li:hover ul {display: block; position: absolute;}\n");
      out.write("            li:hover li {float: none;}\n");
      out.write("            li:hover a {background:#387A31;}\n");
      out.write("            li:hover li a:hover {background: #000;}\n");
      out.write("            #drop-nav li ul li {border-top: 0px;}\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body><div class=\"header\">\n");
      out.write("            \n");
      out.write("            <h1>\n");
      out.write("                <b>Customer Feedback Application</b> </h1>\n");
      out.write("        </div>\n");
      out.write("        <div><ul id=\"drop-nav\">\n");
      out.write("                <li><a href=\"#\">Home</a></li>\n");
      out.write("                <li><a href=\"#\">Customer</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"Register.jsp\">Register</a></li>\n");
      out.write("                        <li><a href=\"index.jsp\">Login</a></li>\n");
      out.write("                       \n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("                <li><a href=\"#\">Feedback</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"#\">Write Feedback</a></li>\n");
      out.write("                        <li><a href=\"#\">View Feedbacks</a></li>\n");
      out.write("                     \n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("                <li><a href=\"#\">Contact</a>\n");
      out.write("                    <ul>\n");
      out.write("                      \n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("            </ul></div>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("         <br>\n");
      out.write("    <center>\n");
      out.write("        <h3> Registration Form </h3></center>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("       \n");
      out.write("        <form method=\"post\" action=\"registration.jsp\">\n");
      out.write("            <center>\n");
      out.write("            <table border=\"1\" width=\"30%\" cellpadding=\"5\" background=\"#356530\">\n");
      out.write("                <thead>\n");
      out.write("                    <tr>\n");
      out.write("                        <th colspan=\"2\">Enter Information Here</th>\n");
      out.write("                    </tr>\n");
      out.write("                </thead>\n");
      out.write("                <tbody>\n");
      out.write("                    <tr>\n");
      out.write("                        <td>First Name</td>\n");
      out.write("                        <td><input type=\"text\" name=\"fname\" value=\"\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td>Last Name</td>\n");
      out.write("                        <td><input type=\"text\" name=\"lname\" value=\"\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td>Email</td>\n");
      out.write("                        <td><input type=\"text\" name=\"email\" value=\"\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td>User Name</td>\n");
      out.write("                        <td><input type=\"text\" name=\"uname\" value=\"\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td>Password</td>\n");
      out.write("                        <td><input type=\"password\" name=\"pass\" value=\"\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td><input type=\"submit\" value=\"Submit\" /></td>\n");
      out.write("                        <td><input type=\"reset\" value=\"Reset\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td colspan=\"2\">Already registered!! <a href=\"index.jsp\">Login Here</a></td>\n");
      out.write("                    </tr>\n");
      out.write("                </tbody>\n");
      out.write("            </table>\n");
      out.write("            </center>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
