<%-- 
    Document   : Register
    Created on : 22-Sep-2016, 11:43:56 AM
    Author     : Claricia
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Customer Feedback Application</title>
       <style type="text/css">
            body{
                background: #54943E ; 
            }
            #header{
                width: 80%;
                height: 100%;
                background:#16A085;
            }
            #container{
                width: 80px; padding:20px;
                height: 150px;
            }
            ul {list-style: none;padding: 0px;margin: 0px;}
            ul li {display: block;position: relative;float: left;border:1px solid #000}
            li ul {display: none;}
            ul li a {display: block;background: #000;padding: 16px 10px 3px 271px;text-decoration: none;
                     white-space: nowrap;color: #fff;}
            ul li a:hover {background: #1F531A   ;}
            li:hover ul {display: block; position: absolute;}
            li:hover li {float: none;}
            li:hover a {background:#387A31;}
            li:hover li a:hover {background: #000;}
            #drop-nav li ul li {border-top: 0px;}
        </style>
    </head>
    <body><div class="header">
            
            <h1>
                <b>Customer Feedback Application</b> </h1>
        </div>
        <div><ul id="drop-nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Customer</a>
                    <ul>
                        <li><a href="Register.jsp">Register</a></li>
                        <li><a href="index.jsp">Login</a></li>
                       
                    </ul>
                </li>
                <li><a href="#">Feedback</a>
                    <ul>
                        <li><a href="#">Write Feedback</a></li>
                        <li><a href="#">View Feedbacks</a></li>
                     
                    </ul>
                </li>
                <li><a href="#">Contact</a>
                    <ul>
                      
                    </ul>
                </li>
            </ul></div>
        <br>
        <br>
        <br>
         <br>
    <center>
        <h3> Registration Form </h3></center>
        <br>
        <br>
       
        <form method="post" action="Registration.jsp" name="frmreg" onSubmit="return validateForm()">
            <center>
            <table border="1" width="30%" cellpadding="5" background="#356530">
                <thead>
                    <tr>
                        <th colspan="2">Enter Information Here</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>First Name</td>
                        <td><input type="text" name="fname" value="" /></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input type="text" name="lname" value="" /></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input type="text" name="email" value="" /></td>
                    </tr>
                    <tr>
                        <td>User Name</td>
                        <td><input type="text" name="uname" value="" /></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="pass" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit" /></td>
                        <td><input type="reset" value="Reset" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">Already registered!! <a href="index.jsp">Login Here</a></td>
                    </tr>
                </tbody>
            </table>
            </center>
        </form>
    </body>
</html>