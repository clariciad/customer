<%-- 
    Document   : sucess
    Created on : 22-Sep-2016, 12:46:51 PM
    Author     : Claricia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Customer Feedback Application</title>
        <style type="text/css">
            body{
                background: #54943E ; 
            }
            #header{
                width: 80%;
                height: 100%;
                background:#16A085;
            }
            #container{
                width: 80px; padding:20px;
                height: 150px;
            }
            ul {list-style: none;padding: 0px;margin: 0px;}
            ul li {display: block;position: relative;float: left;border:1px solid #000}
            li ul {display: none;}
            ul li a {display: block;background: #000;padding: 16px 10px 3px 271px;text-decoration: none;
                     white-space: nowrap;color: #fff;}
            ul li a:hover {background: #1F531A   ;}
            li:hover ul {display: block; position: absolute;}
            li:hover li {float: none;}
            li:hover a {background:#387A31;}
            li:hover li a:hover {background: #000;}
            #drop-nav li ul li {border-top: 0px;}
        </style>
    </head>
    <body>
         <div class="header">
            
            <h1>
                <b>Customer Feedback Application</b> </h1>
        </div>
        <div><ul id="drop-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="#">Customer</a>
                    <ul>
                        <li><a href="Register.jsp">Register</a></li>
                        <li><a href="index.jsp">Login</a></li>
                       
                    </ul>
                </li>
                <li><a href="#">Feedback</a>
                    <ul>
                        <li><a href="FeedbackForm.jsp">Write Feedback</a></li>
                        <li><a href="ViewFeedback.jsp">View Feedbacks</a></li>
                     
                    </ul>
                </li>
                <li><a href="#">Contact</a>
                    <ul>
                      
                    </ul>
                </li>
            </ul></div>
        <br>
        <br>
         <br>
         <br>
<%
    if ((session.getAttribute("userid") == null) || (session.getAttribute("userid") == "")) {
%>
<center>
<h2>You are not logged in</h2><br/>
<a href="index.jsp">Please Login</a>
<%} else {
%>
</center>
    <center>
<b><h2>Welcome </h2>
    <h2> <%=session.getAttribute("userid")%></h2><br>
    <a href="FeedbackForm.jsp"><h3> Write a Feedback</h3></a>
    <br>
   <a href="ViewFeedback.jsp"> <h3>View Feedback</h3></a>
    <a href='logout.jsp'>Log out</a></b>
<%
    }
%>
</center>
    </body>
</html>
