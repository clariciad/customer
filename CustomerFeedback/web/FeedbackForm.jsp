<%-- 
    Document   : FeedbackForm
    Created on : 23-Sep-2016, 11:57:26 AM
    Author     : Claricia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Customer Feedback Application</title>
        
   <script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <style type="text/css">
            body{
                background: #54943E ; 
            }
            #header{
                width: 80%;
                height: 100%;
                background:#16A085;
            }
            #container{
                width: 80px; padding:20px;
                height: 150px;
            }
            ul {list-style: none;padding: 0px;margin: 0px;}
            ul li {display: block;position: relative;float: left;border:1px solid #000}
            li ul {display: none;}
            ul li a {display: block;background: #000;padding: 16px 10px 3px 271px;text-decoration: none;
                     white-space: nowrap;color: #fff;}
            ul li a:hover {background: #1F531A   ;}
            li:hover ul {display: block; position: absolute;}
            li:hover li {float: none;}
            li:hover a {background:#387A31;}
            li:hover li a:hover {background: #000;}
            #drop-nav li ul li {border-top: 0px;}
              table, th , td {
            border: 1px solid grey;
            border-collapse: collapse;
            padding: 5px;
         }
          table, th , td {
            border: 1px solid grey;
            border-collapse: collapse;
            padding: 23px;
         }
         
         table tr:nth-child(odd) {
            background-color: #387A31;
         }
         
         table tr:nth-child(even) {
            background-color: #387A31;
         }
       textarea {
  height:auto;
  max-width:280px;

  font-weight:100;
  font-size:20px;
 
  width:100%;
  background:#fff;
  border-radius:3px;
  line-height:1em;
  border:none;
  box-shadow:0px 0px 5px 1px rgba(0,0,0,0.1);
  padding:10px;
       }
        </style>
         
    </head>
    <body>
          <div class="header">
            
            <h1>
                <b>Customer Feedback Application</b> </h1>
        </div>
        <div><ul id="drop-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="#">Customer</a>
                    <ul>
                        <li><a href="Register.jsp">Register</a></li>
                        <li><a href="index.jsp">Login</a></li>
                       
                    </ul>
                </li>
                <li><a href="#">Feedback</a>
                    <ul>
                        <li><a href="FeedbackForm.jsp">Write Feedback</a></li>
                        <li><a href="ViewFeedback.jsp">View Feedbacks</a></li>
                     
                    </ul>
                </li>
                <li><a href="#">Contact</a>
                    <ul>
                      
                    </ul>
                </li>
            </ul></div>
        <br>
        <br>
    <center>
       
        <h3>Feedback Form</h3>
         <div ng-app = "mainApp" ng-controller = "FeedbackController">
         
         <form name = "feedbackForm" action="feedback1.jsp" method="post" novalidate>
            <table border = "1">
               <tr>
                  <td>Enter First name:</td>
                  <td><input name = "firstname" type = "text" ng-model = "firstName" required>
                     <span style = "color:red" ng-show = "feedbackForm.firstname.$dirty && feedbackForm.firstname.$invalid">
                        <span ng-show = "feedbackForm.firstname.$error.required">First Name is required.</span>
                     </span>
                  </td>
               </tr>
               
               <tr>
                  <td>Enter Last name: </td>
                  <td><input name = "lastname"  type = "text" ng-model = "lastName" required>
                     <span style = "color:red" ng-show = "feedbackForm.lastname.$dirty && feedbackForm.lastname.$invalid">
                        <span ng-show = "feedbackForm.lastname.$error.required">Last Name is required.</span>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>Enter Phone No: </td>
                  <td><input name = "Phoneno"  type = "text" ng-model = "Phoneno" required>
                     <span style = "color:red" ng-show = "feedbackForm.Phoneno.$dirty && feedbackForm.Phoneno.$invalid">
                        <span ng-show = "feedbackForm.Phoneno.$error.required"> Phone no required.</span>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>Email: </td><td><input name = "email" type = "email" ng-model = "email" length = "100" required>
                     <span style = "color:red" ng-show = "feedbackForm.email.$dirty && feedbackForm.email.$invalid">
                        <span ng-show = "feedbackForm.email.$error.required">Email is required.</span>
                        <span ng-show = "feedbackForm.email.$error.email">Invalid email address.</span>
                     </span>
                  </td>
               </tr>
                 <tr>
                  <td>Enter Message: </td>
                  <td><textarea name = "Message"  type = "text"  ng-model="Message" ng-maxlength="100" required></textarea>
                     <span style = "color:red" ng-show = "feedbackForm.Message.$dirty && feedbackForm.Message.$invalid">
                        <span ng-show = "feedbackForm.Message.$error.required"> Message required.</span>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <button ng-click = "reset()">Reset</button>
                  </td>
                  <td>
                     <button ng-disabled = "feedbackForm.firstname.$dirty &&
                        feedbackForm.firstname.$invalid || feedbackForm.lastname.$dirty &&
                        feedbackForm.lastname.$invalid || feedbackForm.Phoneno.$dirty &&
                        feedbackForm.Phoneno.$invalid || feedbackForm.email.$dirty &&
                        feedbackForm.email.$invalid || feedbackForm.Message.$dirty &&
                        feedbackForm.Message.$invalid" ng-click="submit()">Submit</button>
                  </td>
               </tr>
				
            </table>
         </center>
         </form>
      </div>
      
      <script>
         var mainApp = angular.module("mainApp", []);
         
         mainApp.controller('FeedbackController', function($scope) {
            $scope.reset = function(){
               $scope.firstName = "FirstName";
               $scope.lastName = "LastName";
               $scope.Phoneno="Phoneno in Numeric"
               $scope.email = "xxxx@yyyy.com";
               $scope.Message = "Enter Message";
            }
            
            $scope.reset();
         });
      </script>
   
    </body>
</html>
