<%-- 
    Document   : Register
    Created on : 22-Sep-2016, 11:43:56 AM
    Author     : Claricia
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Customer Feedback Application</title>
        <script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
       <style type="text/css">
            body{
                background: #54943E ; 
            }
            #header{
                width: 80%;
                height: 100%;
                background:#16A085;
            }
            #container{
                width: 80px; padding:20px;
                height: 150px;
            }
            ul {list-style: none;padding: 0px;margin: 0px;}
            ul li {display: block;position: relative;float: left;border:1px solid #000}
            li ul {display: none;}
            ul li a {display: block;background: #000;padding: 16px 10px 3px 271px;text-decoration: none;
                     white-space: nowrap;color: #fff;}
            ul li a:hover {background: #1F531A   ;}
            li:hover ul {display: block; position: absolute;}
            li:hover li {float: none;}
            li:hover a {background:#387A31;}
            li:hover li a:hover {background: #000;}
            #drop-nav li ul li {border-top: 0px;}
        </style>
    </head>
    <body><div class="header">
            
            <h1>
                <b>Customer Feedback Application</b> </h1>
        </div>
        <div><ul id="drop-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="#">Customer</a>
                    <ul>
                        <li><a href="Register.jsp">Register</a></li>
                        <li><a href="index.jsp">Login</a></li>
                       
                    </ul>
                </li>
                <li><a href="#">Feedback</a>
                    <ul>
                        <li><a href="FeedbackForm.jsp">Write Feedback</a></li>
                        <li><a href="#">View Feedbacks</a></li>
                     
                    </ul>
                </li>
                <li><a href="#">Contact</a>
                    <ul>
                      
                    </ul>
                </li>
            </ul></div>
        <br>
        <br>
        <br>
         <br>
    <center>
        <h3> Registration Form </h3></center>
        <br>
        <br>
        <div ng-app = "mainApp" ng-controller = "RegisterController">
        <form method="post" action="Registration.jsp" name="frmreg" onSubmit="return validateForm()">
            <center>
            <table border="1" width="30%" cellpadding="5" background="#356530" border-collapse="collapse" >
                <thead>
                    <tr>
                        <th colspan="2">Enter Information Here</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>First Name</td>
                        <td><input type="text" name="fname" value="" ng-model = "Register.firstName" required/></td>
                        <span style = "color:red" ng-show = "Register.firstname.$dirty && Register.firstname.$invalid">
                        <span ng-show = "Register.firstname.$error.required">First Name is required.</span>
                     </span>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input type="text" name="lname" value="" ng-model = "Register.lastName" required/></td>
                        <span style = "color:red" ng-show = "Register.lastname.$dirty && Register.lastname.$invalid">
                        <span ng-show = "Register.lastname.$error.required">Last Name is required.</span>
                     </span>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input  name="email" value="" type = "email" ng-model = "email" required/></td>
                      <span style = "color:red" ng-show = "Register.email.$dirty && Register.email.$invalid">
                        <span ng-show = "Register.email.$error.required">Email is required.</span>
                        <span ng-show = "Register.email.$error.email">Invalid email address.</span>
                     </span>
                    </tr>
                    <tr>
                        <td>User Name</td>
                        <td><input type="text" name="uname" value="" ng-model = "Register.UserName"  required/></td>
                        <span style = "color:red" ng-show = "Register.UserName.$dirty && Register.UserName.$invalid">
                        <span ng-show = "Register.UserName.$error.required">UserName is required.</span>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="pass" value="" ng-model = "Register.Password"  required/></td>
                        <span style = "color:red" ng-show = "Register.Password.$dirty && Register.Password.$invalid">
                        <span ng-show = "Register.Password.$error.required">First Name is required.</span>
                    </tr>
                    <tr>
                        <td>   <button ng-disabled = "Register.firstname.$dirty &&
                        Register.firstname.$invalid || Register.lastname.$dirty &&
                        Register.lastname.$invalid || Register.email.$dirty &&
                        Register.email.$invalid || Register.UserName.$dirty &&
                        Register.UserName.$invalid || Register.Password.$dirty &&
                        Register.Password.$invalid " ng-click="submit()">Submit</button></td>
                        <td><button ng-click = "reset()">Reset</button>
                  </td></td>
                    </tr>
                    <tr>
                        <td colspan="2">Already registered!! <a href="index.jsp">Login Here</a></td>
                    </tr>
                </tbody>
                         <script>
         var mainApp = angular.module("mainApp", []);
         
         mainApp.controller('RegisterController', function($scope) {
            $scope.reset = function(){
               $scope.firstName = "FirstName";
               $scope.lastName = "LastName";
             $scope.Email = "xxxx@yyyy.com";
              $scope.UserName = "Username";
                $scope.Password = "Password";
            }
            
            $scope.reset();
         });
      </script>
            </table>
            </center>
    
        </form>
    </body>
</html>