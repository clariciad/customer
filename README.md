# **FEEDBACK APPLICATION SYSTEM** #

IT IS A WEB APPLICATION IMPLEMENTED FOR USERS TO WRITE FEEDBACK IT IS FOR REGISTERED AND UNREGISTERED USERS FOR THE APPLICATION SYSTEM.USERS CAN ALSO REGISTER FOR THE APPLICATION.
........................................................................................

## DETAILED PROCEDURE ## 

**HARDWARE REQUIREMENTS**
-----------------------
HARD DISK: 500 GB,
RAM: 4 GB,
O.S: WINDOWS 8,
PROCESSOR: CORE I5,

**SOFTWARE REQUIREMENTS:**
----------------------
NETBEANS 8.1,
JDK 1.8.0_71,
NOTEPAD++,
WEB BROWSER:GOOGLE CHROME,
SOURCETREE FOR BITBUCKET,

**TECHNOLOGIES USED**
------------------
JAVA SERVER PAGES,
JAVASCRIPT,
SQL (JAVA DB EMBEDDED),
ANGULARJS,
CSS,

**
CREATE A BITBUCKET ACCOUNT NAME REPOSITORY AS CUSTOMER**,

**--------------------------
HOW TO RUN THE PROJECT IN 
NETBEANS**
 
1)ONCE NETBEANS IS LOADED ---GO TO---FILE----OPEN PROJECT---SEARCH THE FILE IN THE NETBEANS PROJECT(NETBEANS DEFAULT FOLDER..P.N: PLACE YOUR PROJECT INITIALLY IN NETBEANS PROJECT FOLDER),

2)ONCE THE PROJECT IS LOADED IN NETBEANS --SOMETIMES THERE ARE PROBLEMS ON PROJECT---ONE DIALOG BOX WILL APPEAR REGARDING RESOLVING THE PROJECT PROBLEM ---CLICK ON RESOLVE PROBLEM

3) CREATE DATABASE ---GOT TO WINDOWS---SERVICES---
![customer.jpg](https://bitbucket.org/repo/8LrXnb/images/4276479042-customer.jpg)

................

CLICK ON **JAVA DB** --RIGHT CLICK----CREATE **DATABASE**--- NAME IT AS **CUSTOMER**----

![CONF.jpg](https://bitbucket.org/repo/8LrXnb/images/713656957-CONF.jpg)

-----------------------------------------------------------
RIGHT CLICK AND CLICK CREATE TABLE

![CONF1.jpg](https://bitbucket.org/repo/8LrXnb/images/2844453043-CONF1.jpg)
-----------------------------------
CREATE **TABLE** CUSTOMERS----- FOR USERS TO WRITE FEEDBACK

![dATABASE1.jpg](https://bitbucket.org/repo/8LrXnb/images/3847953567-dATABASE1.jpg)

![dATABASE2.jpg](https://bitbucket.org/repo/8LrXnb/images/4214139974-dATABASE2.jpg)
---------------------------------
CREATE **TABLE** MEMBERS---FOR USERS TO REGISTER

![dATABASE3.jpg](https://bitbucket.org/repo/8LrXnb/images/831965153-dATABASE3.jpg)
-------------------------------------------------

GO ON THE PROJECT---TO RUN THE PROJECT------

![RUN.jpg](https://bitbucket.org/repo/8LrXnb/images/2894957605-RUN.jpg)

------------------------------------------
**
WELCOME PAGE**

![dATABASE5.jpg](https://bitbucket.org/repo/8LrXnb/images/1088992367-dATABASE5.jpg)

--------------------------------------------
**NAVIGATE TO REGISTER FOR USERS**

![dATABASE6.jpg](https://bitbucket.org/repo/8LrXnb/images/860418670-dATABASE6.jpg)
---------------------------------------------------------
**REGISTER FORM FOR USERS,**

![Registration form.jpg](https://bitbucket.org/repo/8LrXnb/images/556717753-Registration%20form.jpg)

--------------------------------------------------------



**LOGIN USER PAGE**

![Login form.jpg](https://bitbucket.org/repo/8LrXnb/images/3133950629-Login%20form.jpg)

------------------------------------------------
**WELCOME USER**

![WelcomeUser.jpg](https://bitbucket.org/repo/8LrXnb/images/463559224-WelcomeUser.jpg)

---------------------------------------------------------------

** FEEDBACK FORM**

![Feedback Form.jpg](https://bitbucket.org/repo/8LrXnb/images/2685561777-Feedback%20Form.jpg)

------------------------------------------------
**SUCCESSFULL FEEDBACK**

![FeedbackSuccessFull.jpg](https://bitbucket.org/repo/8LrXnb/images/2378234471-FeedbackSuccessFull.jpg)

---------------------------------------------
**VIEW FEEDBACK FOR USERS**

![ViewFeedback.jpg](https://bitbucket.org/repo/8LrXnb/images/2949063390-ViewFeedback.jpg)



--------------------------------------
**REFERNCES USED**

ANGULAR JS:https://www.tutorialspoint.com/angularjs/angularjs_forms.htm
-------------------------



-------------------END-------------------------------------------END-------------------